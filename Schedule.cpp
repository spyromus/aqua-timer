#include "Schedule.h"

bool hasStoredSchedule();
Schedule restoreSchedule();
void storeSchedule(Schedule schedule);
Schedule createEmptySchedule(bool power);


// Initializes the schedule
Schedule initSchedule(bool power) {
  Schedule schedule;

  if (hasStoredSchedule()) {
    schedule = restoreSchedule();
  } else {
    schedule = createEmptySchedule(power);
  }
  
  return schedule;
}

// Checks if the current power should be ON according to the schedule.
bool isPowerOn(Schedule schedule, int secOfDay) {
  bool power = schedule.initialPower;
  
  for (int i = 0; i < MAX_SLOTS; i++) {
    if (schedule.slots[i].t == UNUSED) {
      return power;
    } else if (schedule.slots[i].t <= secOfDay) {
      power = schedule.slots[i].power;
    }
  }

  return power;
}

DeserializationError loadScheduleFromJson(Schedule &schedule, String input) {
  const int capacity = JSON_ARRAY_SIZE(MAX_SLOTS) + MAX_SLOTS * JSON_OBJECT_SIZE(2);
  StaticJsonDocument<capacity> doc;

  DeserializationError err = deserializeJson(doc, input);
  if (err) {
    return err;
  }

  Serial.println("loading");
  
  for (int s = 0; s < MAX_SLOTS; s++) {
    JsonObject so = doc[s];
    Serial.print("Slot: ");
    Serial.print(s);
    if (so.isNull()) {
      Serial.println(" unused");
      schedule.slots[s].t = UNUSED;
      schedule.slots[s].power = schedule.initialPower;    
    } else {
      Serial.print(" t: ");
      Serial.print(so["t"].as<int>());
      Serial.print(" power: ");
      Serial.println(so["power"].as<bool>() ? "true" : "false");
      schedule.slots[s].t = so["t"];
      schedule.slots[s].power = so["power"];
    }
  }

  storeSchedule(schedule);
  
  return err;
}

void saveScheduleToJson(Schedule schedule, JsonArray array) {
  for (int i = 0; i < MAX_SLOTS; i++) {
    JsonObject obj = array.createNestedObject();
    obj["t"] = schedule.slots[i].t;
    obj["power"] = schedule.slots[i].power;
  }
}

// --- private

// Creates an empty schedule
Schedule createEmptySchedule(bool power) {
  Schedule schedule;

  schedule.initialPower = power;
  for (int i = 0; i < MAX_SLOTS; i++) {
    schedule.slots[i] = { UNUSED, power };
  }

  return schedule;
}

// Checks if the EEPROM has a stored schedule
bool hasStoredSchedule() {
  return EEPROM.read(STORED_FLAG_ADDR) == STORED_FLAG;
}

// Stores schedule to EEPROM
void storeSchedule(Schedule schedule) {
  EEPROM.write(300, 2);
  EEPROM.write(STORED_FLAG_ADDR, STORED_FLAG);
  EEPROM.put(SCHEDULE_ADDR, schedule);
  EEPROM.commit();
}

// Restores the schedule from EEPROM
Schedule restoreSchedule() {
  Schedule schedule;
  EEPROM.get(SCHEDULE_ADDR, schedule);
  return schedule;
}
