#include <ezTime.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ArduinoOTA.h>
#include <ESP8266WebServer.h>
#include <EEPROM.h>
#include "Schedule.h"

const char* ssid = "Noizeramp";
const char* password = "";

ESP8266WebServer server(80);

const char* www_username = "admin";
const char* www_password = "admin";

const int POWER_PIN = 5;
bool powerState;
Schedule schedule;
Timezone TZ;

void dumpSchedule(Schedule s);
int currentSecondOfDay();
int timeToSeconds(int hour, int minute, int second);
void setPower(bool v);

void setup() {
  Serial.begin(115200);

  pinMode(POWER_PIN, OUTPUT);
  setPower(false);
  
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  if(WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("WiFi Connect Failed! Rebooting...");
    delay(1000);
    ESP.restart();
  }
  ArduinoOTA.begin();

  server.on("/", handleRoot);
  server.on("/set-schedule", handleSetSchedule);
  server.begin();

  Serial.print("Open http://");
  Serial.print(WiFi.localIP());
  Serial.println("/ in your browser to see it working");

  if (!TZ.setCache(0)) TZ.setLocation("Europe/Moscow");
  TZ.setDefault();

  waitForSync();

  // Start eeprom after loading TZ as loading TZ calls EEPROM.end()
  EEPROM.begin(512);

  schedule = initSchedule(false);
  setCurrentPower();

  setInterval(60);
  setDebug(INFO);
}

// Checks authentication
bool authenticated() {
  if (!server.authenticate(www_username, www_password)) {
    server.requestAuthentication();
    return false;
  } else {
    return true;
  }
}

// Handles /
void handleRoot() {
  if (!authenticated()) return;

  StaticJsonDocument<500> doc;
  doc["status"] = "OK";
  doc["power"] = powerState;
  
  JsonArray array = doc.createNestedArray("schedule");
  saveScheduleToJson(schedule, array);
  
  String body;
  serializeJson(doc, body);

  server.send(200, "application/json", body);
}

// Handles /set-schedule
void handleSetSchedule() {
  if (!authenticated()) return;

  DeserializationError err = loadScheduleFromJson(schedule, server.arg("plain"));
  dumpSchedule(schedule);
  if (err) {
    String msg = "{\"status\":\"ERROR\",\"msg\":\"";
    msg += err.c_str();
    msg += "\"}";
    server.send(500, "application/json", msg);
  } else {
    server.send(200, "application/json", "{\"status\":\"OK\"}");
  }
}

void setCurrentPower() {
  int sec = currentSecondOfDay();
  setPower(isPowerOn(schedule, sec));
}

void loop() {
  setCurrentPower();
  ArduinoOTA.handle();
  server.handleClient();
}

// ----------

// Returns current time of day in seconds
int currentSecondOfDay() {
  return timeToSeconds(TZ.hour(), TZ.minute(), TZ.second());
}

// Converts time to seconds
int timeToSeconds(int hour, int minute, int second) {
  return hour * 3600 + minute * 60 + second;
}

// Dumps current schedule to Serial for debug purposes
void dumpSchedule(Schedule s1) {
  Serial.println("--- Schedule ---");
  for (int i = 0; i < MAX_SLOTS; i++) {
    Serial.print(i, DEC);
    Serial.print(" sec: ");
    Serial.print(s1.slots[i].t, DEC);
    Serial.print(" state: ");
    Serial.println(s1.slots[i].power ? "true" : "false");
  }
}

// Sets power on / off
void setPower(bool v) {
  digitalWrite(POWER_PIN, v ? HIGH : LOW);
  powerState = v;
}
