#ifndef Schedule_h
#define Schedule_h

#include <EEPROM.h>
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>

#define MAX_SLOTS 10
#define UNUSED -1

#define STORED_FLAG      0x4A
#define STORED_FLAG_ADDR 100
#define SCHEDULE_ADDR    101

struct Slot {
  int t;
  bool power;
};

struct Schedule {
  bool initialPower;
  Slot slots[MAX_SLOTS];
};

// Initializes the schedule
Schedule initSchedule(bool power);

// Checks if the current state should be ON according to the schedule.
bool isPowerOn(Schedule schedule, int _t);

// Loads schedule from the json input
DeserializationError loadScheduleFromJson(Schedule &schedule, String input);

// Saves schedule to JSON document
void saveScheduleToJson(Schedule schedule, JsonArray array);
#endif
